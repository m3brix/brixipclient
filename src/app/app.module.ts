import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddPremissionComponent } from './Components/add-premission/add-premission.component';
import { FormsModule} from '@angular/forms'
import { RouterModule, Routes } from '@angular/router';
import {InputTextModule} from  'primeng/inputtext';
import {ButtonModule} from  'primeng/button';
import {TableModule} from 'primeng/table';
import {SliderModule} from 'primeng/slider';
import {MultiSelectModule} from 'primeng/multiselect';
import 'w3-css/w3.css';
import { ReactiveFormsModule } from '@angular/forms';
import { IpList1Component } from './Components/ip-list1/ip-list1.component';

// import {DropdownModule} from 'primeng/dropdown';
// import {ListboxModule} from 'primeng/listbox';
// import {FileUploadModule} from 'primeng/fileupload';
// import {StepsModule} from 'primeng/steps'
// import {SpinnerModule} from 'primeng/spinner';
// import {ChartModule} from 'primeng/chart';
// import {RadioButtonModule} from 'primeng/radiobutton';

const routes: Routes = [
 
  {
    path: 'AddPremission',
    component: AddPremissionComponent

  },
  {
    path: 'IpList1',
    component: IpList1Component

  }

];


@NgModule({
  declarations: [
    AppComponent,
    AddPremissionComponent,
    IpList1Component,
  ],
  imports: [
    BrowserModule,InputTextModule,ButtonModule,
    SliderModule,MultiSelectModule,
    RouterModule.forRoot(routes,{enableTracing: true}),
    HttpClientModule,
    FormsModule,
    TableModule,
    BrowserAnimationsModule,
    ReactiveFormsModule 

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
