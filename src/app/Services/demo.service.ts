import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Person } from '../person';
@Injectable({
  providedIn: 'root'
})
export class DemoService {
BASIC_URL="/api/Demo/";
  constructor(private http:HttpClient) { }
  getStart():Observable<string>{
    return this.http.get<string>(this.BASIC_URL);
  }
  addPerson(person:Person):Observable<string>{
    return this.http.post<string>(this.BASIC_URL,person);
  }
}
