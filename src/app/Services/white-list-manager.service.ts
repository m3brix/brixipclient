import { Injectable } from '@angular/core';
import { IPDetails } from '../Classes/ipdetails';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WhiteListManagerService {
  IpList: IPDetails[] = [];
  BASIC_URL = "/api/Demo/";
  isShow:boolean
  constructor(private http: HttpClient) { }
  addIpPremission(IpDetails: IPDetails): Observable<any> {
    debugger;
    return this.http.post("services/api/Values/AddWhiteIP", IpDetails);
  }
  UpdateIpPremission(ip: IPDetails) {
    debugger;
    return this.http.put("services/api/Values/EditDescreption",ip)
  }
  DeleteIpPremission(Ip: IPDetails) {
    debugger
    return this.http.delete("services/api/Values/DeleteWhiteIp/"+ Ip.ip)
  }
  GetIpPremissionsList() {
    debugger;
    return this.http.get("services/api/values/GetIp")
  }
  tryout():Observable<string>
{

return this.http.get<string>("services/api/values/Get")
}
// 
}