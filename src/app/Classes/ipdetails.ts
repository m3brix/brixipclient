export class IPDetails {
    /**
     *
     */
    constructor(
        public id: number,
        public ip: string,
        public description: string
    ) {
    }

}
