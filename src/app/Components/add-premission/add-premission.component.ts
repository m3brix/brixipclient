import { Component, OnInit } from '@angular/core';
import { IPDetails } from 'src/app/Classes/ipdetails';
import { WhiteListManagerService } from 'src/app/Services/white-list-manager.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-premission',
  templateUrl: './add-premission.component.html',
  styleUrls: ['./add-premission.component.css']
})
export class AddPremissionComponent implements OnInit {

  constructor(public whiteListManagerService: WhiteListManagerService,private location:Location,private router:Router) { }
  newIpDetails:IPDetails
  premission:FormGroup
  load:boolean
  isShow:boolean
  get f() { return this.premission.controls; }
REGEX="^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
  ngOnInit() {
    this.isShow=true
    document.getElementById('id01').style.display='block';
    this.premission = new FormGroup({
      ip: new FormControl('', [Validators.required, Validators.pattern(this.REGEX)]),
      desc:new FormControl('')
    })
  }
  addIpPremission() {
    debugger;
    this.newIpDetails=new IPDetails(0,this.f.ip.value,this.f.desc.value)
    this.whiteListManagerService.addIpPremission(this.newIpDetails).subscribe(
      data=>{console.log(data);
      this.whiteListManagerService.GetIpPremissionsList().subscribe( (data: IPDetails[]) => {
          console.log(data);
          this.whiteListManagerService.IpList = data;
          this.load = true
        })
    
      },
      // data =>{this.whiteListManagerService.IpList.push(this.newIpDetails)},
      err=>console.log("לא הצלחתי להוסיף IP לרשימה הלבנה!!!"),
      ()=>this.location.back()
    )
  }
  close()
    {
      this.whiteListManagerService.isShow=false
    }
  
}
