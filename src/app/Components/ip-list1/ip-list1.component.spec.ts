import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IpList1Component } from './ip-list1.component';

describe('IpList1Component', () => {
  let component: IpList1Component;
  let fixture: ComponentFixture<IpList1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IpList1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IpList1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
