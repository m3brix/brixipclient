import { Component, OnInit } from '@angular/core';
import { IPDetails } from 'src/app/Classes/ipdetails';
import { WhiteListManagerService } from 'src/app/Services/white-list-manager.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ip-list1',
  templateUrl: './ip-list1.component.html',
  styleUrls: ['./ip-list1.component.css']
})
export class IpList1Component implements OnInit {
  clonedIps: { [s: string]: IPDetails; } = {};
  lonedIps: { [s: string]: IPDetails; } = {};
  IP_REGEX = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
  load: boolean;
  deleting: boolean = false
  constructor(public wlservice: WhiteListManagerService,private location:Router ) { }

  ngOnInit() {
    this.wlservice.GetIpPremissionsList().subscribe((data: IPDetails[]) => {
      console.log(data);
      this.wlservice.IpList = data;
      this.load = true
    })

  }
  ngAfterViewInit()
  {

  }
  onRowEditInit(ip: IPDetails) {
    this.clonedIps[ip.id] = { ...ip };
  }
  onRowEditSave(Ip: IPDetails) {
    if (Ip.ip) {
      delete this.clonedIps[Ip.id];
      this.wlservice.UpdateIpPremission(Ip).subscribe(
        data => console.log("EDIT SUCCESS"),
        err => console.log("edit error")
      )
    }
    else {
    }
  }
  Delete(ip: IPDetails) {
    this.deleting = false;
debugger
    this.wlservice.DeleteIpPremission(ip).subscribe(data => { this.wlservice.IpList.splice(this.wlservice.IpList.indexOf(ip), 1) }
    )
  }
  onRowEditCancel(ip: IPDetails, index: number) {
    this.wlservice.IpList[index] = this.clonedIps[ip.id];
    delete this.clonedIps[ip.id];

  }
add(){
  this.wlservice.isShow=true
  this.location.navigate(['/AddPremission'])
}
debugg()
{
  alert("In here")
  this.wlservice.tryout().subscribe(data=>{alert(data)},err=>{alert(JSON.stringify(err))});
  
}
}
